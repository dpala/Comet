proc min { a b } {
    if { $a < $b } {
         return $a
    } else {
         return $b
    }
}
proc max { a b } {
    if { $a > $b } {
         return $a
    } else {
         return $b
    }
}
